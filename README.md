# MultipleColor

組員分工 紀棋峰-企劃及程式設計 陳冠臻-Ui設計 溫茈櫻-主視覺設計 盧昱瑄-影片及PPT設計

## 主視覺及遊戲介紹

![主視覺](https://gitlab.com/h5921512/multiplecolor/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%871.PNG?inline=false)
![理念](https://gitlab.com/h5921512/multiplecolor/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%872.PNG?inline=false)
![理念](https://gitlab.com/h5921512/multiplecolor/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%873.PNG?inline=false)
![玩法](https://gitlab.com/h5921512/multiplecolor/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%874.PNG?inline=false)

## 遊戲玩法

需要先與電腦端連接相同的Wifi，透過Osc協定傳輸來運行，選好顏色後按下錄音按鈕，會啟動電腦的預設麥克風錄音一秒

玩家即可錄製球體的語音，之後點擊UI左上角按鍵進入下一步，按下Start即可將球丟入電腦的實驗室內。

 
