﻿using UnityEngine;
using UnityEngine.UI;

using extOSC;
public class Test : MonoBehaviour
{

    Vector3 accelerationdir;

    public string address;

    public string coloraddress;

    

    public OSCTransmitter Transmitter;

    bool sendbeford=false;

    Color ballcolor;

    bool cliked = false;


    public Image ballcolorimg;

    protected virtual void Start()
    {
               
        Send(address, OSCValue.String("i am working"));
    }

    void Update()
    {
        accelerationdir = Input.acceleration;

        if (accelerationdir.sqrMagnitude >= 4f && cliked)
        {
            if (!sendbeford)
            {
                Send(address, OSCValue.String("creatit"));
                
                sendbeford = true;
            }
  
        }

    }

    public void sendreset()
    {
        Send(address, OSCValue.String("reset"));
    }

    public void useingmic()
    {
        Send(address, OSCValue.String("recording"));
    }
    public void sendcolorvalue()
    {
        cliked = true;
        ballcolor = ballcolorimg.GetComponent<Image>().color;
        Send(coloraddress, OSCValue.Color(ballcolor)); 
    }
    private void Send(string address, OSCValue value)
    {
        var message = new OSCMessage(address, value);

        Transmitter.Send(message);
    }

    

}

