﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControll : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource soundeffect;
    public AudioSource soundeffect2;
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            soundeffect.Play();
        }
        else if (other.tag == "Player2")
        {
            soundeffect2.Play();
        }
        
    }
}
