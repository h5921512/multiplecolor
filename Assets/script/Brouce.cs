﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using extOSC;


public class Brouce : MonoBehaviour
{

    float[] power = new float[] {50,-50 };

    public int broucetimes = 0;

    float fixx, fixy, fixz;

    float colorcounts=255f;

    

    Color ballscolor;

    public string address="";

    //public OSCReceiver Receiver;

    string savemessage="ha";

    bool firstime = true;

    int getRandomvalue()
    {
       return Random.Range(0, 2);
        
    }
    
    void Start()
    {
       ballscolor= gameObject.GetComponent<Renderer>().material.color;
       //Receiver.Bind(address, ReceivedMessage);
       
    }

    /*private void ReceivedMessage(OSCMessage message)
    {
        Debug.LogFormat("Received: {0}", message);

        if (message.ToString(out var value))
        {
            savemessage = value;
            if (savemessage == "throwing")
            {
                forward();
                savemessage = "";
            }            
        }        
    }*/


    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            forward();

        }
         
    }
    public void brounceing()
    {
        
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(fixx, fixy, fixz);
        colorcounts = colorcounts - 3.1f;
        gameObject.GetComponent<Renderer>().material.color = new Color(ballscolor.r, ballscolor.g, ballscolor.b, colorcounts / 255f);

    }
    public void forward()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0, 0, 80);
    }
    void OnTriggerEnter(Collider other)
    {
      
        broucetimes++;
        fixx = power[getRandomvalue()];
        fixy = power[getRandomvalue()]; 
        fixz = power[getRandomvalue()];
        if (!firstime)
        {
            if (broucetimes < 50)
            {

                if (other.tag == "back")
                {
                    fixz = -50;
                    brounceing();
                }
                else if (other.tag == "right")
                {
                    fixx = -50;
                    brounceing();
                }
                else if (other.tag == "left")
                {
                    fixx = 50;
                    brounceing();
                }
                else if (other.tag == "ground")
                {
                    fixy = 50;
                    brounceing();
                }
                else if (other.tag == "roof")
                {
                    fixy = -50;
                    brounceing();
                }
                else if (other.tag == "face")
                {
                    fixz = 50;
                    brounceing();
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }
       
    }
    private void OnTriggerExit(Collider other)
    {
        gameObject.GetComponent<Collider>().isTrigger = false;
        firstime = false;
    }


}
