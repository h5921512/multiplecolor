﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class Recorder2 : MonoBehaviour
{
    public float minThreshold = 0;
    public float frequency = 0.0f;
    public int audioSampleRate = 44100;
    public string microphone;
    public FFTWindow fftWindow;



    private int samples = 8192;
    private AudioSource audioSource;

    void Start()
    {


        //get components you'll need
        audioSource = GetComponent<AudioSource>();




    }

    public void UpdateMicrophone2()
    {
        audioSource.Stop();
        //Start recording to audioclip from the mic
        audioSource.clip = Microphone.Start(microphone, false, 1, audioSampleRate);
        audioSource.loop = false;
        // Mute the sound with an Audio Mixer group becuase we don't want the player to hear it
        Debug.Log(Microphone.IsRecording(microphone).ToString());

        if (Microphone.IsRecording(microphone))
        { //check that the mic is recording, otherwise you'll get stuck in an infinite loop waiting for it to start
            while (!(Microphone.GetPosition(microphone) > 0))
            {
            } // Wait until the recording has started. 

            Debug.Log("recording started with " + microphone);

            // Start playing the audio source
            audioSource.Play();
        }
        else
        {
            //microphone doesn't work for some reason

            Debug.Log(microphone + " doesn't work!");
        }
    }


    public float GetAveragedVolume()
    {
        float[] data = new float[256];
        float a = 0;
        audioSource.GetOutputData(data, 0);
        foreach (float s in data)
        {
            a += Mathf.Abs(s);
        }
        return a / 256;
    }

    public float GetFundamentalFrequency()
    {
        float fundamentalFrequency = 0.0f;
        float[] data = new float[samples];
        audioSource.GetSpectrumData(data, 0, fftWindow);
        float s = 0.0f;
        int i = 0;
        for (int j = 1; j < samples; j++)
        {
            if (data[j] > minThreshold) // volumn must meet minimum threshold
            {
                if (s < data[j])
                {
                    s = data[j];
                    i = j;
                }
            }
        }
        fundamentalFrequency = i * audioSampleRate / samples;
        frequency = fundamentalFrequency;
        return fundamentalFrequency;
    }
}
