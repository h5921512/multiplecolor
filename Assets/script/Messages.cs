﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
using System;


namespace message
{
    public struct Notification : NetworkMessage
    {
        public string content;
    }


    public class Messages : MonoBehaviour
    {
        // Start is called before the first frame update

        void Start()
        {
            if (!NetworkClient.active) { return; }
            NetworkClient.RegisterHandler<Notification>(OnNotification);
         
        }
        

        // Update is called once per frame
        public void OnNotification(NetworkConnection connect, Notification msg)
        {
            Debug.Log("OnScoreMessage " + msg.content);

        }

    }
}
