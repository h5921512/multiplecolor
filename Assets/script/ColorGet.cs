﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ColorGet : MonoBehaviour
{

    public Image getimage;
    public Image setimage;
    Color32 getcolor;
    Color32 setcolor;

    public void getcolors()
    {
        getimage.GetComponent<Image>().color = getcolor;
        setcolor = getcolor;    
        setimage.GetComponent<Image>().color = setcolor;
    }

    public void changescene()
    {
        SceneManager.LoadScene(1);
    }
    

}
