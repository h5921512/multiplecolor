﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using extOSC;
using UnityEngine.SceneManagement;
public class Ballcreater : MonoBehaviour
{
    // Start is called before the first frame update
    public OSCReceiver Receiver;

    public string creatball;

    public GameObject ballprefeb;

    public GameObject ballprefeb2;

    string savemessage;

    public Material ballmaterial;

    public Material ballmaterial2;

    public string coloraddress = "";

    public string coloraddress2 = "";

    public Recorder rec;

    public Recorder2 rec2;

    protected virtual void Start()
    {
        Receiver.Bind(creatball, ReceivedMessage);
        Receiver.Bind(coloraddress, ReceiveColor);
        Receiver.Bind(coloraddress2, ReceiveColor2);
    }


    int randspawn()
    {
        return Random.Range(-13, 11);
    }
    private void ReceivedMessage(OSCMessage message)
    {
        Debug.LogFormat("Received: {0}", message);
        
        if (message.ToString(out var value))
        {
            savemessage = value;
            Debug.Log(savemessage);
            if (savemessage == "creatit")
            {
                var ballobject = Instantiate(ballprefeb, new Vector3(randspawn(), 4.6f, -65.9f), Quaternion.identity);
                ballobject.GetComponent<Brouce>().forward();
            }
            if (savemessage == "creatit2")
            {
                var ballobject2 = Instantiate(ballprefeb2, new Vector3(randspawn(), 4.6f, -65.9f), Quaternion.identity);
                ballobject2.GetComponent<Brouce>().forward();
            }
            if (savemessage == "reset")
            {
                SceneManager.LoadScene("SampleScene");
            }
            if (savemessage == "recording")
            {
                rec.UpdateMicrophone();
            }
            if (savemessage == "recording2")
            {
                rec2.UpdateMicrophone2();
            }
            
        }
    }
    public void ReceiveColor(OSCMessage message)
    {
        Debug.Log("i get the color value");
        if (message.ToColor(out var value))
        {
            Debug.Log(value);
            ballmaterial.color = value;
        }
    }
    public void ReceiveColor2(OSCMessage message)
    {
        Debug.Log("i get the color value");
        if (message.ToColor(out var value))
        {
            Debug.Log(value);
            ballmaterial2.color = value;
        }
    }
}
