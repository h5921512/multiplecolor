<Q                         DIRECTIONAL    VERTEXLIGHT_ON      D  #ifdef VERTEX
#version 310 es

in highp vec4 in_POSITION0;
in highp vec4 in_TANGENT0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
in highp vec4 in_TEXCOORD2;
in highp vec4 in_TEXCOORD3;
in mediump vec4 in_COLOR0;
layout(location = 0) out highp vec4 vs_TANGENT0;
layout(location = 1) out highp vec3 vs_NORMAL0;
layout(location = 2) out highp vec4 vs_TEXCOORD0;
layout(location = 3) out highp vec4 vs_TEXCOORD1;
layout(location = 4) out highp vec4 vs_TEXCOORD2;
layout(location = 5) out highp vec4 vs_TEXCOORD3;
layout(location = 6) out mediump vec4 vs_COLOR0;
void main()
{
    gl_Position = in_POSITION0;
    vs_TANGENT0 = in_TANGENT0;
    vs_NORMAL0.xyz = in_NORMAL0.xyz;
    vs_TEXCOORD0 = in_TEXCOORD0;
    vs_TEXCOORD1 = in_TEXCOORD1;
    vs_TEXCOORD2 = in_TEXCOORD2;
    vs_TEXCOORD3 = in_TEXCOORD3;
    vs_COLOR0 = in_COLOR0;
    return;
}

#endif
#ifdef FRAGMENT
#version 310 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ZBufferParams;
uniform 	vec4 hlslcc_mtx4x4unity_CameraToWorld[4];
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform 	float _Radius;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(1) uniform highp sampler2D unity_NHxRoughness;
UNITY_LOCATION(2) uniform mediump samplerCube unity_SpecCube0;
layout(location = 0) in highp vec2 gs_TEXCOORD0;
layout(location = 1) in highp vec2 gs_TEX0;
layout(location = 2) in highp vec3 gs_TEXCOORD2;
layout(location = 3) in highp vec3 gs_VIEWPOS0;
layout(location = 0) out highp vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
int u_xlati2;
bool u_xlatb2;
vec3 u_xlat3;
vec3 u_xlat4;
mediump vec4 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
mediump vec3 u_xlat16_8;
mediump vec3 u_xlat16_9;
mediump vec3 u_xlat16_10;
float u_xlat11;
bool u_xlatb12;
float u_xlat22;
float u_xlat23;
float u_xlat33;
float u_xlat34;
bool u_xlatb34;
mediump float u_xlat16_38;
mediump float u_xlat16_39;
mediump float u_xlat16_40;
void main()
{
    u_xlat0.xy = gs_TEX0.xy * vec2(2.0, 2.0) + vec2(-1.0, -1.0);
    u_xlat0.z = 0.0;
    u_xlat1.xy = u_xlat0.xy * vec2(1.5, 1.5);
    u_xlat33 = dot(gs_VIEWPOS0.xyz, gs_VIEWPOS0.xyz);
    u_xlat33 = inversesqrt(u_xlat33);
    u_xlat2.xyz = vec3(u_xlat33) * gs_VIEWPOS0.xyz;
    u_xlat2.xyz = u_xlat2.xyz / vec3(_Radius);
    u_xlat33 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat23 = dot(u_xlat2.xy, u_xlat1.xy);
    u_xlat34 = u_xlat23 + u_xlat23;
    u_xlat1.x = dot(u_xlat1.xy, u_xlat1.xy);
    u_xlat1.x = u_xlat1.x + -1.0;
    u_xlatb12 = u_xlat33!=0.0;
    u_xlatb2 = u_xlat23!=0.0;
    u_xlatb12 = u_xlatb12 && u_xlatb2;
    u_xlati2 = ~((int(u_xlatb12) * int(0xffffffffu)));
    u_xlat1.x = u_xlat33 * u_xlat1.x;
    u_xlat1.x = u_xlat1.x * 4.0;
    u_xlat1.x = u_xlat34 * u_xlat34 + (-u_xlat1.x);
    u_xlatb34 = u_xlat1.x<0.0;
    u_xlatb34 = u_xlatb34 && u_xlatb12;
    if(u_xlatb34){discard;}
    u_xlat1.x = sqrt(u_xlat1.x);
    u_xlat1.x = (-u_xlat23) * 2.0 + (-u_xlat1.x);
    u_xlat33 = u_xlat33 + u_xlat33;
    u_xlat33 = u_xlat1.x / u_xlat33;
    if((u_xlati2)!=0){discard;}
    u_xlat33 = u_xlatb12 ? u_xlat33 : float(0.0);
    u_xlat1.z = u_xlat33 * (-u_xlat2.z);
    u_xlat33 = (-u_xlat1.z) * _Radius + gs_VIEWPOS0.z;
    u_xlat33 = float(1.0) / u_xlat33;
    u_xlat33 = u_xlat33 + (-_ZBufferParams.w);
    gl_FragDepth = u_xlat33 / _ZBufferParams.z;
    u_xlat1.x = float(0.0);
    u_xlat1.y = float(0.0);
    u_xlat0.xyz = u_xlat0.xyz * vec3(1.5, 1.5, 1.5) + (-u_xlat1.xyz);
    u_xlat33 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat33 = inversesqrt(u_xlat33);
    u_xlat0.xyz = vec3(u_xlat33) * u_xlat0.xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_CameraToWorld[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4unity_CameraToWorld[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_CameraToWorld[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    u_xlat1.xyz = (-gs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat33 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat33 = inversesqrt(u_xlat33);
    u_xlat1.xyz = vec3(u_xlat33) * u_xlat1.xyz;
    u_xlat16_2.xyz = texture(_MainTex, gs_TEXCOORD0.xy).xyz;
    u_xlat3.xyz = u_xlat16_2.xyz * _Color.xyz;
    u_xlat4.z = (-_Glossiness) + 1.0;
    u_xlat16_5.x = dot((-u_xlat1.xyz), u_xlat0.xyz);
    u_xlat16_5.x = u_xlat16_5.x + u_xlat16_5.x;
    u_xlat16_5.xyz = u_xlat0.xyz * (-u_xlat16_5.xxx) + (-u_xlat1.xyz);
    u_xlat16_38 = (-u_xlat4.z) * 0.699999988 + 1.70000005;
    u_xlat16_38 = u_xlat4.z * u_xlat16_38;
    u_xlat16_38 = u_xlat16_38 * 6.0;
    u_xlat16_5 = textureLod(unity_SpecCube0, u_xlat16_5.xyz, u_xlat16_38);
    u_xlat16_6.x = u_xlat16_5.w + -1.0;
    u_xlat16_6.x = unity_SpecCube0_HDR.w * u_xlat16_6.x + 1.0;
    u_xlat16_6.x = log2(u_xlat16_6.x);
    u_xlat16_6.x = u_xlat16_6.x * unity_SpecCube0_HDR.y;
    u_xlat16_6.x = exp2(u_xlat16_6.x);
    u_xlat16_6.x = u_xlat16_6.x * unity_SpecCube0_HDR.x;
    u_xlat16_6.xyz = u_xlat16_5.xyz * u_xlat16_6.xxx;
    u_xlat33 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat33 = inversesqrt(u_xlat33);
    u_xlat0.xyz = vec3(u_xlat33) * u_xlat0.xyz;
    u_xlat16_7.xyz = u_xlat16_2.xyz * _Color.xyz + vec3(-0.0399999991, -0.0399999991, -0.0399999991);
    u_xlat16_7.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_7.xyz + vec3(0.0399999991, 0.0399999991, 0.0399999991);
    u_xlat16_39 = (-_Metallic) * 0.959999979 + 0.959999979;
    u_xlat33 = dot(u_xlat1.xyz, u_xlat0.xyz);
    u_xlat34 = u_xlat33 + u_xlat33;
    u_xlat1.xyz = u_xlat0.xyz * (-vec3(u_xlat34)) + u_xlat1.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
    u_xlat33 = u_xlat33;
    u_xlat33 = clamp(u_xlat33, 0.0, 1.0);
    u_xlat11 = dot(u_xlat1.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat16_40 = (-u_xlat33) + 1.0;
    u_xlat11 = u_xlat11 * u_xlat11;
    u_xlat22 = u_xlat16_40 * u_xlat16_40;
    u_xlat22 = u_xlat16_40 * u_xlat22;
    u_xlat4.x = u_xlat11 * u_xlat11;
    u_xlat11 = u_xlat16_40 * u_xlat22;
    u_xlat16_40 = (-u_xlat16_39) + _Glossiness;
    u_xlat16_40 = u_xlat16_40 + 1.0;
    u_xlat16_40 = clamp(u_xlat16_40, 0.0, 1.0);
    u_xlat22 = texture(unity_NHxRoughness, u_xlat4.xz).x;
    u_xlat22 = u_xlat22 * 16.0;
    u_xlat16_8.xyz = u_xlat16_7.xyz * vec3(u_xlat22);
    u_xlat16_8.xyz = u_xlat3.xyz * vec3(u_xlat16_39) + u_xlat16_8.xyz;
    u_xlat16_9.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat16_10.xyz = (-u_xlat16_7.xyz) + vec3(u_xlat16_40);
    u_xlat16_7.xyz = vec3(u_xlat11) * u_xlat16_10.xyz + u_xlat16_7.xyz;
    u_xlat16_6.xyz = u_xlat16_6.xyz * u_xlat16_7.xyz;
    u_xlat16_6.xyz = u_xlat16_8.xyz * u_xlat16_9.xyz + u_xlat16_6.xyz;
    SV_Target0.xyz = u_xlat16_6.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif
#ifdef GEOMETRY
#version 310 es
#ifdef GL_ARB_geometry_shader
#extension GL_ARB_geometry_shader : enable
#endif
#ifdef GL_OES_geometry_shader
#extension GL_OES_geometry_shader : enable
#endif
#ifdef GL_EXT_geometry_shader
#extension GL_EXT_geometry_shader : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_WorldToCamera[4];
uniform 	vec4 hlslcc_mtx4x4unity_CameraToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
uniform 	float _Radius;
 struct _Points_type {
	uint[4] value;
};

layout(std430, binding = 0) readonly buffer _Points {
	_Points_type _Points_buf[];
};
 struct _Indices_type {
	uint[1] value;
};

layout(std430, binding = 1) readonly buffer _Indices {
	_Indices_type _Indices_buf[];
};
layout(location = 0) in highp vec4 vs_TANGENT0 [1];
layout(location = 1) in highp vec3 vs_NORMAL0 [1];
layout(location = 2) in highp vec4 vs_TEXCOORD0 [1];
layout(location = 3) in highp vec4 vs_TEXCOORD1 [1];
layout(location = 4) in highp vec4 vs_TEXCOORD2 [1];
layout(location = 5) in highp vec4 vs_TEXCOORD3 [1];
layout(location = 6) in mediump vec4 vs_COLOR0 [1];
vec4 u_xlat0;
int u_xlati0;
vec4 u_xlat1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
vec4 u_xlat5;
vec4 u_xlat6;
vec3 u_xlat8;
float u_xlat23;
layout(points) in;
layout(triangle_strip) out;
layout(location = 0) out highp vec2 gs_TEXCOORD0;
layout(location = 1) out highp vec2 gs_TEX0;
layout(location = 4) out mediump vec3 gs_TEXCOORD1;
layout(location = 5) out highp float gs_MASS0;
layout(location = 2) out highp vec3 gs_TEXCOORD2;
layout(location = 6) out highp vec4 gs_TEXCOORD6;
layout(location = 3) out highp vec3 gs_VIEWPOS0;
layout(max_vertices = 4) out;
void main()
{
    u_xlati0 = int(_Indices_buf[gl_PrimitiveIDIn].value[(0 >> 2) + 0]);
    u_xlat0 = vec4(uintBitsToFloat(_Points_buf[u_xlati0].value[(0 >> 2) + 0]), uintBitsToFloat(_Points_buf[u_xlati0].value[(0 >> 2) + 1]), uintBitsToFloat(_Points_buf[u_xlati0].value[(0 >> 2) + 2]), uintBitsToFloat(_Points_buf[u_xlati0].value[(0 >> 2) + 3]));
    u_xlat1.x = _Radius * 1.5;
    u_xlat8.xyz = u_xlat1.xxx * hlslcc_mtx4x4unity_CameraToWorld[0].xyz + u_xlat0.xyz;
    u_xlat2.xyz = (-u_xlat1.xxx) * hlslcc_mtx4x4unity_CameraToWorld[1].xyz + u_xlat8.xyz;
    u_xlat8.xyz = u_xlat1.xxx * hlslcc_mtx4x4unity_CameraToWorld[1].xyz + u_xlat8.xyz;
    u_xlat3 = u_xlat2.yyyy * hlslcc_mtx4x4unity_WorldToObject[1];
    u_xlat3 = hlslcc_mtx4x4unity_WorldToObject[0] * u_xlat2.xxxx + u_xlat3;
    u_xlat3 = hlslcc_mtx4x4unity_WorldToObject[2] * u_xlat2.zzzz + u_xlat3;
    u_xlat3 = u_xlat3 + hlslcc_mtx4x4unity_WorldToObject[3];
    u_xlat4 = u_xlat3.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat4 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat3.xxxx + u_xlat4;
    u_xlat4 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat3.zzzz + u_xlat4;
    u_xlat3.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * u_xlat3.www + u_xlat4.xyz;
    u_xlat4 = u_xlat4 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat5 = u_xlat4.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat5 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat4.xxxx + u_xlat5;
    u_xlat5 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat4.zzzz + u_xlat5;
    u_xlat4 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat4.wwww + u_xlat5;
    gl_Position = u_xlat4;
    u_xlat4.xy = vs_TEXCOORD0[0].xy * _MainTex_ST.xy + _MainTex_ST.zw;
    gs_TEXCOORD0.xy = u_xlat4.xy;
    gs_TEX0.xy = vec2(1.0, 0.0);
    u_xlat5.xyz = hlslcc_mtx4x4unity_CameraToWorld[2].yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat5.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * hlslcc_mtx4x4unity_CameraToWorld[2].xxx + u_xlat5.xyz;
    u_xlat5.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * hlslcc_mtx4x4unity_CameraToWorld[2].zzz + u_xlat5.xyz;
    u_xlat6.x = dot(u_xlat5.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat6.y = dot(u_xlat5.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat6.z = dot(u_xlat5.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat23 = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlat23 = inversesqrt(u_xlat23);
    u_xlat5.xyz = vec3(u_xlat23) * u_xlat6.xyz;
    gs_TEXCOORD1.xyz = u_xlat5.xyz;
    gs_TEXCOORD2.xyz = u_xlat3.xyz;
    gs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat3.xyz = u_xlat2.yyy * hlslcc_mtx4x4unity_WorldToCamera[1].xyz;
    u_xlat2.xyw = hlslcc_mtx4x4unity_WorldToCamera[0].xyz * u_xlat2.xxx + u_xlat3.xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_WorldToCamera[2].xyz * u_xlat2.zzz + u_xlat2.xyw;
    u_xlat2.xyz = u_xlat2.xyz + hlslcc_mtx4x4unity_WorldToCamera[3].xyz;
    gs_VIEWPOS0.xyz = u_xlat2.xyz;
    gs_MASS0 = u_xlat0.w;
    EmitVertex();
    u_xlat2 = u_xlat8.yyyy * hlslcc_mtx4x4unity_WorldToObject[1];
    u_xlat2 = hlslcc_mtx4x4unity_WorldToObject[0] * u_xlat8.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_WorldToObject[2] * u_xlat8.zzzz + u_xlat2;
    u_xlat2 = u_xlat2 + hlslcc_mtx4x4unity_WorldToObject[3];
    u_xlat3 = u_xlat2.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat3 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat2.xxxx + u_xlat3;
    u_xlat3 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat2.zzzz + u_xlat3;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * u_xlat2.www + u_xlat3.xyz;
    u_xlat3 = u_xlat3 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat6 = u_xlat3.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat6 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat3.xxxx + u_xlat6;
    u_xlat6 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat3.zzzz + u_xlat6;
    u_xlat3 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat3.wwww + u_xlat6;
    gl_Position = u_xlat3;
    gs_TEXCOORD0.xy = u_xlat4.xy;
    gs_TEX0.xy = vec2(1.0, 1.0);
    gs_TEXCOORD1.xyz = u_xlat5.xyz;
    gs_TEXCOORD2.xyz = u_xlat2.xyz;
    gs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat2.xyz = u_xlat8.yyy * hlslcc_mtx4x4unity_WorldToCamera[1].xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_WorldToCamera[0].xyz * u_xlat8.xxx + u_xlat2.xyz;
    u_xlat8.xyz = hlslcc_mtx4x4unity_WorldToCamera[2].xyz * u_xlat8.zzz + u_xlat2.xyz;
    u_xlat8.xyz = u_xlat8.xyz + hlslcc_mtx4x4unity_WorldToCamera[3].xyz;
    gs_VIEWPOS0.xyz = u_xlat8.xyz;
    gs_MASS0 = u_xlat0.w;
    EmitVertex();
    u_xlat0.xyz = (-u_xlat1.xxx) * hlslcc_mtx4x4unity_CameraToWorld[0].xyz + u_xlat0.xyz;
    u_xlat8.xyz = (-u_xlat1.xxx) * hlslcc_mtx4x4unity_CameraToWorld[1].xyz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat1.xxx * hlslcc_mtx4x4unity_CameraToWorld[1].xyz + u_xlat0.xyz;
    u_xlat2 = u_xlat8.yyyy * hlslcc_mtx4x4unity_WorldToObject[1];
    u_xlat2 = hlslcc_mtx4x4unity_WorldToObject[0] * u_xlat8.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_WorldToObject[2] * u_xlat8.zzzz + u_xlat2;
    u_xlat2 = u_xlat2 + hlslcc_mtx4x4unity_WorldToObject[3];
    u_xlat3 = u_xlat2.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat3 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat2.xxxx + u_xlat3;
    u_xlat3 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat2.zzzz + u_xlat3;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * u_xlat2.www + u_xlat3.xyz;
    u_xlat3 = u_xlat3 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat6 = u_xlat3.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat6 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat3.xxxx + u_xlat6;
    u_xlat6 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat3.zzzz + u_xlat6;
    u_xlat3 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat3.wwww + u_xlat6;
    gl_Position = u_xlat3;
    gs_TEXCOORD0.xy = u_xlat4.xy;
    gs_TEX0.xy = vec2(0.0, 0.0);
    gs_TEXCOORD1.xyz = u_xlat5.xyz;
    gs_TEXCOORD2.xyz = u_xlat2.xyz;
    gs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat2.xyz = u_xlat8.yyy * hlslcc_mtx4x4unity_WorldToCamera[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToCamera[0].xyz * u_xlat8.xxx + u_xlat2.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToCamera[2].xyz * u_xlat8.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToCamera[3].xyz;
    gs_VIEWPOS0.xyz = u_xlat1.xyz;
    gs_MASS0 = u_xlat0.w;
    EmitVertex();
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_WorldToObject[1];
    u_xlat1 = hlslcc_mtx4x4unity_WorldToObject[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_WorldToObject[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_WorldToObject[3];
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat2 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat1.zzzz + u_xlat2;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * u_xlat1.www + u_xlat2.xyz;
    u_xlat2 = u_xlat2 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat3 = u_xlat2.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat3 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat2.xxxx + u_xlat3;
    u_xlat3 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat2.zzzz + u_xlat3;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat2.wwww + u_xlat3;
    gl_Position = u_xlat2;
    gs_TEXCOORD0.xy = u_xlat4.xy;
    gs_TEX0.xy = vec2(0.0, 1.0);
    gs_TEXCOORD1.xyz = u_xlat5.xyz;
    gs_TEXCOORD2.xyz = u_xlat1.xyz;
    gs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_WorldToCamera[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToCamera[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToCamera[2].xyz * u_xlat0.zzz + u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToCamera[3].xyz;
    gs_VIEWPOS0.xyz = u_xlat0.xyz;
    gs_MASS0 = u_xlat0.w;
    EmitVertex();
    return;
}

#endif
 �                             $Globals�   
      _WorldSpaceCameraPos                         _ZBufferParams                          _WorldSpaceLightPos0                  `      unity_SpecCube0_HDR                   p      _LightColor0                  �      _Glossiness                   �   	   _Metallic                     �      _Color                    �      _Radius                   �      unity_CameraToWorld                             $GlobalsT        _MainTex_ST                   @     _Radius                   P     unity_WorldToCamera                         unity_CameraToWorld                  @      unity_ObjectToWorld                  �      unity_WorldToObject                  �      unity_MatrixVP                                _MainTex                  unity_NHxRoughness                  unity_SpecCube0                 _Points               _Indices          